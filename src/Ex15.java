import java.util.Scanner;

public class Ex15 {
    public static void main(String[] args) {
        double var1;
        double var2;
        double temp;

        Scanner input = new Scanner(System.in);

        System.out.print("Input var1 ");
        var1 = input.nextDouble();

        System.out.print("Input var2 ");
        var2 = input.nextDouble();

        temp = var1;
        var1 = var2;
        var2 = temp;

        System.out.println("var1 = "+ var1);
        System.out.println("var2 = "+ var2);
    }
}

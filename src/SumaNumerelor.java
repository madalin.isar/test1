public class SumaNumerelor {

    public static final int MAX = 100;

    public static void main(String[] args) {

        int suma = 0;
        for (int numar = 1; numar <= MAX; numar++){
            if (numar % 2 == 0)
                continue;

            suma = adunaDoiIntregi(suma, numar);

        }
        System.out.println("suma numerelor este:" + suma);
    }


    public static int adunaDoiIntregi (int n1, int n2){
        int result =  n1 + n2;
        return result;
    }
}

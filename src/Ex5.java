public class Ex5 {
    public static void main(String[] args) {
        int firstNumber = 25;
        int secondNumber = 5;
        int result = firstNumber * secondNumber;
        String resultAsString = "25 x 5 = " +  result;
        System.out.println(resultAsString);
    }
}

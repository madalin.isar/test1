package Oop;

public class Telefon {
    String brand;
    double diagonala = 5.5;
    boolean areHusa = false;
    boolean esteBlocatTelefonul = true;

    public void deblocareTelefon (){
        esteBlocatTelefonul = false;
        System.out.println("telefonul s-a deblocat");
    }
    public void trimitereMesaj (String mesaj){
        if (esteBlocatTelefonul == true) {
            deblocareTelefon();
        }
        System.out.println("Trimite "+mesaj);
    }
}

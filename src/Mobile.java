public class Mobile {

    public static void main(String[] args) {


        String [] catalog = new String[]{"Ana", "are", "aere"};
        boolean success = isPageCorrect(catalog,'a');
        System.out.println(success);
    }

    private static boolean isPageCorrect(String[] catalog, char selectedLetter) {
        boolean success = true;
        for (int i = 0; i< catalog.length; i++){
            String name = catalog[i];
            char firstLetter = name.charAt(0);

            if (isRightLetter(firstLetter, selectedLetter)){
                success = false;
                break;
            }

        }
        return success;
    }

    private static boolean isRightLetter(char firstLetter, char selectedLetter) {
        return Character.toLowerCase(firstLetter) != selectedLetter;
    }
}

public class Ex6 {
    public static void main(String[] args) {
        int firstNumber = 125;
        int secondNumber = 24;
        int sum = firstNumber + secondNumber;
        int diff = firstNumber - secondNumber;
        int product = firstNumber * secondNumber;
        int devide = firstNumber / secondNumber;
        int remainder = firstNumber % secondNumber;

        String sumAsString = firstNumber + " + " + secondNumber + " = " + sum;
        String diffAsString = firstNumber + " - " + secondNumber + " = " + diff;
        String productAsString = firstNumber + " * " + secondNumber + " = " + product;
        String devideAsString = firstNumber + " / " + secondNumber + " = " + devide;
        String remainderAsString = firstNumber + " mod " + secondNumber + " = " + remainder;
        System.out.println(sumAsString);
        System.out.println(diffAsString);
        System.out.println(productAsString);
        System.out.println(devideAsString);
        System.out.println(remainderAsString);
    }
}

import java.util.Scanner;

public class Ex49 {
    public static void main(String[] args) {
        System.out.print("Input a number: ");
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        if (x % 2 ==0) System.out.println(1);
        else System.out.println(0);
    }
}

Feature: Form Testing
  Scenario: I want to fill in the form
    Given the user accesses the form
    And he fills in his personal data
    When the user submits the form
    Then the user is informed that he successfully submitted the form
    
Feature: Testing the Settings
  Testing the Settings functionality

  @smokeTest
  Scenario: As a user I want to change the camera device.

    Given The user is logged in.
    When I click the Settings button
    And I click on the camera option
    Then I should be able to see available devices
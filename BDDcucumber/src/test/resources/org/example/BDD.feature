Feature: Code refactoring
  Usage of OOP principals in test automation

  Scenario: As a tester I want to use a clean code
    Given Code is OOP organised
    When New steps are implemented
    Then Test should be passed
    * extra check
    * check polymorphism
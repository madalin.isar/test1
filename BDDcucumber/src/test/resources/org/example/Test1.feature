Feature: Testing BlueJeans
  Testing the main functionalities of BlueJeans.

  @smokeTest
  Scenario: As a user, when I join the conference I want to be able to share the screen.

    Given The user is logged in.
    Given The microphone button is displayed.
    When The user clicks share screen button.
    And The user clicks microphone button.
    Then The sharing options menu should be displayed.

  Scenario: As a user, when I join the conference, I want to see that people are online.

    Given The user is logged in.
    When I click the People button
    Then I will be able to see the people present in the conference

    @smokeTest @chat
  Scenario: As a user I want to be able to write in chat

    Given The user is logged in.
    When I click the Chat button
    And I write in chat: Hello and press Enter
    Then What I wrote should be displayed in chat

      @regression
    Scenario: As a user I want to activate the microphone and see who is present

      Given The user is logged in.
      When The user clicks microphone button.
      And I click the People button
      Then I will be able to see the people present in the conference
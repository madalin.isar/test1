Feature: Website title
  I want to test the website title with Selenium
  Scenario: As a user I want to check the website title so that title checks the requirements

    Given Website is loaded
    When I read the title
    Then Title should be correct
    * Close browser window
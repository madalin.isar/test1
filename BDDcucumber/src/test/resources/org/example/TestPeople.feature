Feature: Presence of people
  Testing the presence of people.

  Scenario Outline: As a student, when I click people section, I want to see the students name

    Given "<name>" joined the conference
    When I click the People button
    Then "<nickname>" should be on the list
    And <CNP> should be displayed
  Examples:
  | name | nickname | CNP |
  | Danut | Danut A |1    |
  |Daniel | Daniel  |2    |
  |Vasile | Vasile  |3    |
  |Madalin | Madalin |4   |
  |Gabriel | Gabriel |5   |
  |Alina   |Alina    |6   |
  |Alex    |Alex     |7   |



#
#  Scenario: As a student, when I click people section, I want to see the students name
#
#    Given Daniel joined the conference
#    When I click the People button
#    Then Daniel should be on the list
#
#
#  Scenario: As a student, when I click people section, I want to see the students name
#
#    Given Vasile joined the conference
#    When I click the People button
#    Then Vasile should be on the list
#
#
#  Scenario: As a student, when I click people section, I want to see the students name
#
#    Given Madalin joined the conference
#    When I click the People button
#    Then Madalin should be on the list
#




package org.example;

import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.After;

import static org.junit.Assert.*;

public class StepDefinitions {

//    @Before
//    public void testHook(){
//        System.out.println("Driver initialisation");
//    }
//
//    @BeforeStep
//    public void turnOnInternet(){
//        System.out.println("Connected to the Internet");
//    }
//
//    @After
//    public void closeConnection(){
//        System.out.println("Connection closed");
//    }

    @Given ("The user is logged in.")
    public void logging() {
        System.out.println("The user is logged in.");
    }
    @Given ("{string} joined the conference")
    public void joinConference(String name) {
        System.out.println(name + " joined the conference");
    }

    @And ("The microphone button is displayed.")
    public void enablingSound(){
        System.out.println("The microphone button is displayed.");
    }

    @When("The user clicks share screen button.")
    public void sharingScreen(){
        System.out.println("The user clicks share screen button.");
    }

    @When("I click the Chat button")
    public void chatButton(){
        System.out.println("I click the Chat button");
    }

    @When("I click the People button")
    public void peopleButton(){
        System.out.println("I click the People button");
    }

    @When("I click the Settings button")
    public void settingsButton(){
        System.out.println("I click the Settings button");
    }

    @And("I click on the camera option")
    public void cameraOption(){
        System.out.println("I click on the camera option");
    }

    @And("The user clicks microphone button.")
    public void soundOptions(){
        System.out.println("The user clicks microphone button.");
    }

    @And("I write in chat: Hello and press Enter")
    public void writeInChat(){
        System.out.println("I write in chat: Hello and press Enter");
    }

    @Then("The sharing options menu should be displayed.")
    public void checkingOptions(){
        assertEquals("IntelliJ view","IntelliJ view");
    }

    @Then("I will be able to see the people present in the conference")
    public void peoplePresent(){
        System.out.println("I will be able to see the people present in the conference");
    }

    @Then("What I wrote should be displayed in chat")
    public void displayedInChat(){
        System.out.println("What I wrote should be displayed in chat");
    }

    @Then("I should be able to see available devices")
    public void availableDevicesCamera(){
        System.out.println("I should be able to see available devices");
    }

    @Then("{string} should be on the list")
    public void listOfPeople(String nickname){
        System.out.println(nickname + " should be on the list");
    }

    @And("{int} should be displayed")
    public void cnpCheck(int cnp){
        System.out.println(cnp + " should be displayed");
    }
}

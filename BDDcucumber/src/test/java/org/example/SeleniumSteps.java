package org.example;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;

public class SeleniumSteps {
    WebDriver driver;

    @Given("Website is loaded")
    public void loadWebsite(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        this.driver = new ChromeDriver();
    }

    @When("I read the title")
    public void readWebsite(){
        this.driver.get("http://www.airbnb.com");

    }

    @Then("Title should be correct")
    public void checkTitle(){
        String title = this.driver.getTitle();
        assertEquals("Vacation Rentals, Homes, Experiences & Places - Airbnb", title);
    }

    @And("Close browser window")
    public void closeWindow(){
        this.driver.close();
    }
}

package org.example;

import static org.junit.Assert.assertEquals;

abstract class TestModels {
    abstract void setup();

    abstract void condition();

    abstract void check();

    protected void extraCheck(){
        assertEquals("tc_name", "tc_name");
    }
    private void privateCheck(){
        assertEquals("tc_name", "tc_name");
    }

}

package org.example;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
//@CucumberOptions(features = {"src/test/resources/org/example/Test1.feature",
//        "src/test/resources/org/example/Test2.feature",
//        "src/test/resources/org/example/TestPeople.feature"},
//        plugin = {"pretty", "json:target/raport.json",
//                "html:target/cucumber-reports.html"})
////        tags = "@smokeTest and @chat")
////        tags = "(not @smokeTest) and (@chat or @regression)")

@CucumberOptions(features = {"src/test/resources/org/example/Form.feature"} ,
        plugin = {"pretty", "json:target/raport.json", "html:target/cucumber-reports.html"})
public class RunCucumberTest {

}

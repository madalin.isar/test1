package org.example.page;

import com.sun.org.apache.bcel.internal.generic.Select;
import lombok.Getter;
import lombok.Setter;
import org.example.selenium.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class FormPage{
    WebDriver driver;


    public FormPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(id = "first-name")
    private WebElement firstNameInput;

    @FindBy(id = "last-name")
    private WebElement lastNameInput;

    @FindBy(id = "job-title")
    private WebElement jobTitleInput;

    @FindBy(id = "radio-button-1")
    private WebElement educationRadioButtonhighschool;

    @FindBy(id = "radio-button-2")
    private WebElement educationRadioButtoncollege;

    @FindBy(id = "radio-button-3")
    private WebElement educationRadioButtongradschool;

    @FindBy(id = "checkbox-1")
    private WebElement sexcheckboxmale;

    @FindBy(id = "checkbox-2")
    private WebElement sexcheckboxfemale;

    @FindBy(id = "checkbox-3")
    private WebElement sexcheckboxunknown;

    @FindBy(id = "select-menu")
    private Select yearsExperiencedropdown;

    @FindBy(id = "datepicker")
    private WebElement dateInput;

    public void submitForm(WebDriver driver){
        getFirstNameInput().sendKeys("Ionel");
        getLastNameInput().sendKeys("Ionelule");
    }

   // private WebElement firstRadioEducation = this.driver.findElement(By.id("radio-button-1"));


//    public WebElement getFirstNameInput() {
//        return firstNameInput;
//    }
//
//    public void setFirstNameInput(WebElement firstNameInput) {
//        this.firstNameInput = firstNameInput;
//    }
//
//    public WebElement getFirstRadioEducation() {
//        return firstRadioEducation;
//    }
//
//    public void setFirstRadioEducation(WebElement firstRadioEducation) {
//        this.firstRadioEducation = firstRadioEducation;
//    }
}

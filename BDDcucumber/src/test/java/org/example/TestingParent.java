package org.example;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestingParent extends TestModels{
    @Given("Code is OOP organised")
    public void setup() {
        System.out.println("setup");
    }

    @When("New steps are implemented")
    public void condition() {
        System.out.println("condition");

    }

    @Then("Test should be passed")
    public void check() {
        System.out.println("check");

    }

    @And("extra check")
    public void newStep(){
        this.extraCheck();
    }

    @And("check polymorphism")
    public void polymorphism(){
        TestTemplate tc1 = new Test1();
        TestTemplate tc2 = new Test2();
        tc1.stepOne();
        tc1.stepTwo();
    }
}

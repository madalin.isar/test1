package org.example.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ScrollToElement {
    WebDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        this.driver = new ChromeDriver();
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep (2000);
        this.driver.quit();
    }

    @Test
    public void scrollToElement() throws InterruptedException {

        this.driver.get("https://formy-project.herokuapp.com/scroll");

        WebElement name = this.driver.findElement(By.id("name"));
        Actions scroll = new Actions(this.driver);

        scroll.moveToElement(name);
        name.sendKeys("Gigel");

        WebDriverWait wait = new WebDriverWait(this.driver,5);
        WebElement date = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("date")));
     //   WebElement date = this.driver.findElement(By.id("date"));
        date.sendKeys("01/01/2001");
        Thread.sleep (1000);

    }
}

package org.example.selenium;

import io.cucumber.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleSearch {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com");
        WebElement searchField = driver.findElement(By.name("q") );
        Thread.sleep (1000);
        searchField.sendKeys("mancare");
        Thread.sleep (1000);
        searchField.submit();
        Thread.sleep (1000);
        driver.quit();

    }
}

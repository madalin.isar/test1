package org.example.selenium;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class SwitchWindow extends BaseTest {
    @Test
    public void myTest() throws InterruptedException {
        this.driver.get("https://formy-project.herokuapp.com/switch-window");
        WebElement newTabButton = this.driver.findElement(By.id("new-tab-button"));
        newTabButton.click();

        String originalHandle = this.driver.getWindowHandle();

        for (String handle1: this.driver.getWindowHandles()) {
            driver.switchTo().window(handle1);
        }
        Thread.sleep(3000);
        this.driver.switchTo().window(originalHandle);

        Thread.sleep(3000);

        WebElement alertButton = this.driver.findElement(By.id("alert-button"));
        alertButton.click();

        Alert alert = this.driver.switchTo().alert();
        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Thread.sleep(3000);
        alert.dismiss();
    }
}

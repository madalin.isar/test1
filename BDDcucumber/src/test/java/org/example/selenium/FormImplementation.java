package org.example.selenium;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.page.FormPage;
import org.example.selenium.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FormImplementation {
    WebDriver driver;
    FormPage formpage = new FormPage(driver);

    @Given("the user accesses the form")
    public void theUserAccessesTheForm() {
//        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
//        this.driver = new ChromeDriver();
        this.driver.get("https://formy-project.herokuapp.com/form");
    }

    @And("he fills in his personal data")
    public void heFillsInHisPersonalData() {
      formpage.getFirstNameInput().sendKeys("Ion");
    }

    @When("the user submits the form")
    public void theUserSubmitsTheForm() {
    }

    @Then("the user is informed that he successfully submitted the form")
    public void theUserIsInformedThatHeSuccessfullySubmittedTheForm() {
    }
}

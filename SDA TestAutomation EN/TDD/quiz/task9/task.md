# Unit tests - naming convention 

To make your code easier to read and understand, it is worth adopting one of the naming conventions and sticking to it. We will not find among the recommended conventions:

a) MethodName_StateUnderTest_ExpectedBehavior

b) MethodName_ExpectedBehavior_StateUnderTest

c) Should_ExpectedBehavior_When_StateUnderTest

d) When_TestedClass_AssertWhat_ExpectedFailure

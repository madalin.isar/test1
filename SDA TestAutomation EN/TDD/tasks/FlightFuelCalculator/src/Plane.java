public interface Plane {

    /**
     * MTOW is a maximum weight allowed for a plane to take off
     * @return value in kg - maximum take off weight
     */
    public double mtow();

    /**
     * ZFW - zero fuel weight, weight of an empty plane with all operation fluids (i.e. oil)
     * @return value in kg - zfw
     */
    public double zfw();

    /**
     * Base (calculated for an empty plane) fuel consumption given in kg/100kg
     * @return fuel consumption in kg/100km
     */
    public double fuelConsumption100km();

    /**
     * Additional distance required to mainain fuel reserve
     * @return distance in km
     */
    public int reserveDistance();

}

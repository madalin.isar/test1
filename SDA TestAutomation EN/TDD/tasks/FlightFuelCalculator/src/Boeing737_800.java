public class Boeing737_800 implements Plane {
    private static final double MTOW = 78245;
    private static final double EMPTY_WEIGHT = 41413;

    // private static final int RANGE = 7400; // not used yet, for info purposes
    private static final int CRUISE_SPEED = 842;

    // fuel consumption is calculated based on 850gph (gallons per hour) and cruise speed
    // 1g = 3.78l
    // 1l ~= 0.7kg
    private static final double FUEL_CONSUMPTION_100KM = 267.11;   // kg/100km


    @Override
    public double mtow() {
        return MTOW;
    }

    @Override
    public double zfw() {
        return EMPTY_WEIGHT;
    }

    @Override
    public double fuelConsumption100km() {
        return FUEL_CONSUMPTION_100KM;
    }

    @Override
    public int reserveDistance() {
        return CRUISE_SPEED / 2;
    }
}

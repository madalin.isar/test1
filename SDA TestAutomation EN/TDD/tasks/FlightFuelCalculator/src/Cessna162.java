public class Cessna162 implements Plane {

    private static final double MTOW = 600;
    private static final int EMPTY_WEIGHT = 376;

    // private static final int RANGE = 870; // not used yet, for info purposes
    private static final int CRUISE_SPEED = 207;

    // fuel consumption is calculated based on 5.5gph (gallons per hour) and cruise speed
    // 1g = 3.78l
    // 1l ~= 0.7kg
    private static final double FUEL_CONSUMPTION_100KM = 7.03;   // kg/100km


    @Override
    public double mtow() {
        return MTOW;
    }

    @Override
    public double zfw() {
        return EMPTY_WEIGHT;
    }

    @Override
    public double fuelConsumption100km() {
        return FUEL_CONSUMPTION_100KM;
    }

    @Override
    public int reserveDistance() {
        // distance fo 30min flight
        return CRUISE_SPEED / 2;
    }
}

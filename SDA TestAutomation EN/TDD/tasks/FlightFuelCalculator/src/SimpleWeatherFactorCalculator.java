public class SimpleWeatherFactorCalculator implements WeatherFactorCalculator {

    /**
     * According to basic specification, below +/-15m has no influence on consumption, above -
     * increases/decreases by 10% (0.1)
     * @param windSpeed - wind speed in m/s,
     *                  positive value - wind has the opposite direction as planned flight,
     *                  negative value - wind has the same direction as planned flight
     * @return
     */
    @Override
    public double windFactor(double windSpeed) {

        // hint:
        // 1. consider using Math.abs
        // 2. consider using Integer.signum

        return 0;
    }
}

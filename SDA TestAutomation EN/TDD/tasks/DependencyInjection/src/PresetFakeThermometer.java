public class PresetFakeThermometer implements Thermometer {

    private double presetTemperature;

    @Override
    public double readTemperature() {
        return presetTemperature;
    }

    public void setPresetTemperature(double newTemperature) {
        presetTemperature = newTemperature;
    }
}

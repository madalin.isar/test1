package test;
import org.junit.Test;
import static io.restassured.RestAssured.given;

public class PolarThicketTest {
    private String baseUrl = "https://polar-thicket-63660.herokuapp.com";
    private String categoryEndpoint = "/api/v1/categories/";
    private String customerEndpoint = "/api/v1/customers";
    private String vendorsEndpoint = "/api/v1/vendors";

    @Test
    public void checkCategories(){
        given().when().get(baseUrl + categoryEndpoint)
                .then().statusCode(200);
    }

    @Test
    public void checkVendors(){
        given().when().get(baseUrl + vendorsEndpoint)
                .then().statusCode(200);
    }

    @Test
    public void createVendor(){
        String vendorPayload = "{\n" +
                "  \"vendorName\": \"Altex\"\n" +
                "}";
        given().when().contentType("application/json").body(vendorPayload).post(baseUrl + vendorsEndpoint)
                .then().statusCode(201);
    }
}
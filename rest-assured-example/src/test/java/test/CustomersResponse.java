package test;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Data
@Setter
public class CustomersResponse {
    private List<CustomerResponse> customers;
}

package test;

import io.restassured.response.Response;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.StringContains.containsString;

public class CustomerTest {
    private CustomersResponse customersResponse;
    private String baseUrl = "https://polar-thicket-63660.herokuapp.com";
    private String customerEndpoint = "/api/v1/customers";

    @Test
    public void checkCustomer(){
        given().when().get(baseUrl + customerEndpoint)
                .then().statusCode(200);
    }
    @Test
    public void createCustomer(){
        String customerPayload = "{\n" +
                "    \"firstName\": \"Cristian\",\n" +
                "    \"lastName\": \"Padure\"\n" +
                "}";
        given().when().contentType("application/json").body(customerPayload).post(baseUrl + customerEndpoint)
                .then().statusCode(201);

    }

    @Test
    public void checkCreatedCustomers (){
        Response responseAllCustomers = given().when().get(baseUrl + customerEndpoint).then()
                .extract().response();
        String customerUrl = responseAllCustomers.path("customers.customerUrl[0]");
        String[] customerData = customerUrl.split("/");
        String customerId = customerData[4];
        System.out.println(customerData[4]);
        given().when().get(baseUrl + customerEndpoint + "/" + customerId).then()
                .statusCode(200);
    }
    
}

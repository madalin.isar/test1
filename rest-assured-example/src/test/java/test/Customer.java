package test;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Data
@Setter
public class Customer {
    private String lastName;
    private String firstName;
}

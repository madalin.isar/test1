package test;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Data
@Setter
public class CustomerResponse {
    private Customer customer;
    private String customerUrl;
}

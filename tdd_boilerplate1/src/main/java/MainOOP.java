public class MainOOP {
    public static void main(String[] args) {
        Child child = new Child();
        child.a();
        child.b();
        child.c();
        System.out.println("Child "+ child.name);
        MyModel myModel = new MyModel();
        myModel.modelType();
        myModel.modelYear();
    }

}

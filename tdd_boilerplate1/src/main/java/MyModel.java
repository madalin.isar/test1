public class MyModel extends Abstract{
    public void modelType (){
        System.out.println("Abstract method implemented");
    }

    @Override
    public void modelYear() {
        System.out.println("Second abstract method implemented");
    }
}

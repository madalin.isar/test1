public class Merchandise {
    private String name;
    private double basePrice;

    public Merchandise(String name, double basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public String getName() {
        return name;
    }

    /**
     * Sets new base price for merchandise
     * @param newBasePrice new price to replace the old one
     */
    public void setNewBasePrice(double newBasePrice) {
        basePrice = newBasePrice;
    }
    public void setNewMerchandiseName(String newMerchandisename){name =newMerchandisename;} 
}



//1.de implementat metoda update name
/*2. in MerchandiseTests - test pentru updateName pozitiv,
test pentru updateName negativ,
test pentru UpdateName cu null ca parametru
 */
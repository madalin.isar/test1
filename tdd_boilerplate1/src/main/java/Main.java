public class Main {
    public static void main(String[] args) {
        ThisKeyword thisKeyword = new ThisKeyword(18, "Ionut", "Ionut@gmail");
        ThisKeyword secondKeyword = new ThisKeyword(19, "Dan", "Dan@gmail");
        System.out.println("Student A is "+ thisKeyword.name + " with mail " + thisKeyword.mail);
        System.out.println("Student B is " + thisKeyword.name);
        thisKeyword.setNickName("Jon");
        String nickName = thisKeyword.getNickName();
        System.out.println("Student A nickname is "+ nickName);
    }
}

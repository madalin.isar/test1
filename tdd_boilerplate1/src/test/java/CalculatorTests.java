import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTests {

    @Test
    public void verificaAddReturneazaZeroPentruEmptyString(){
        Calculator calculator = new Calculator();
        int result = calculator.add("");
        assertEquals(0,result);
    }

    @Test
    public void verificaAddReturneazaNumarPentruNumar(){
        Calculator calculator = new Calculator();
        int result = calculator.add("2");
        assertEquals(2,result);
    }

    @Test
    public void verificaAddReturneazaSumaADouaNumere(){
        Calculator calculator = new Calculator();
        int firstNumber = 23;
        int secondNumber = 6;
        int thirdNumber = -15;
        int suma = firstNumber + secondNumber + thirdNumber;
        String intToString1 = Integer.toString(firstNumber);
        String intToString2 = Integer.toString(secondNumber);
        String intToString3 = Integer.toString(thirdNumber);
        int result = calculator.add(intToString1+","+intToString2+","+intToString3);

        assertEquals(suma,result);
    }
}

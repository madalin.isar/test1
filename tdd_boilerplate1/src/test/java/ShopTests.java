import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ShopTests {

    @Test
    public void checkIfShopNameIsCorrectlyAssigned() {
        Shop shop = new Shop("O'Briens Veggies");

        assertEquals("O'Briens Veggies", shop.getShopName());
    }

    @Test
    public void checkIfIsShopOpen(){
        Shop shop = new Shop("test");
        boolean magazinDeshis = shop.isShopOpen("luni");
        assertEquals(true, magazinDeshis);

    }

    @Test
    public void checkIfIsShopClosed(){
        Shop shop = new Shop("test");
        boolean magazinDeshis = shop.isShopOpen("sambata");
        assertEquals(false, magazinDeshis);

    }
    

    @Test
    public void addMerchandiseAndCheckOnStockSuccess() {
        Shop shop = new Shop("test");

        shop.addMerchandise("Onion", 2.1);
        boolean isOnStock = shop.isOnStock("Onion");

        assertTrue(isOnStock);
    }

    @Test
    public void addMerchandiseAndCheckOnStockFail() {
        Shop shop = new Shop("test");
        shop.addMerchandise("Cartof", 4.2);
        boolean isOnStock = shop.isOnStock("Tomato");
        assertFalse(isOnStock);
    }

    @Test
    public void addMerchandiseAndCheckOnStockCaseInsensitiveSuccess() {
        Shop shop = new Shop("test");

        shop.addMerchandise("Onion", 2.1);
        boolean isOnStock = shop.isOnStock("onion");

        assertTrue(isOnStock);
    }


    @Test
    public void addMerchandiseAndCheckItsPrice() {
        Shop shop = new Shop("test");
        shop.addMerchandise("Onion", 2.1);
        double price = shop.getMerchandisePrice("Onion");
        assertEquals(2.1,price);

    }

    @Test
    public void addMerchandiseAndCheckItsPriceIsDifferent() {
        Shop shop = new Shop("test");
        shop.addMerchandise("Onion", 2.1);
        double price = shop.getMerchandisePrice("Onion");
        assertNotEquals(0.0, price);

    }



    @Test
    public void updateMerchandisePrice() {
        Shop shop = new Shop("test");
        shop.addMerchandise("Onion", 2.1);
        shop.updateMerchandisePrice("Onion", 2.2);
        double newPrice = shop.getMerchandisePrice("Onion");
        assertEquals(2.2,newPrice);

    }

    @Test
    public void updateMerchandisePrice_notFoundMerchandise() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            Shop shop = new Shop("test");
            shop.addMerchandise("Onion", 2.1);
            shop.updateMerchandisePrice("Potato", 2.2);


            double newPrice = shop.getMerchandisePrice("Potato");

            assertEquals(2.2, newPrice);
        });
    }

    @Test
    public void updateMerchandisePriceNullName() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
        Shop shop = new Shop("test");
        shop.addMerchandise("", 2.1);
        });

    }

}